package org.exemple.App;

import org.example.App.Eception.InvalidIsbnLengthExeption;
import org.example.App.Model.Book;
import org.example.App.Specific.IsbnValidator;
import org.junit.jupiter.api.Test;
import org.junit.jupiter.api.extension.ExtendWith;
import org.junit.runner.RunWith;
import org.mockito.Mock;
import org.mockito.junit.MockitoJUnitRunner;
import org.mockito.junit.jupiter.MockitoExtension;

import static org.junit.jupiter.api.Assertions.*;
import static org.mockito.Mockito.verify;

@ExtendWith(MockitoExtension.class)
public class IsbnValidatorTest {

    @Mock
    private IsbnValidator mockIsbnValidator;

    @Test
    public void check_Valid10Char_ISBNCode_IsValid(){
        IsbnValidator isbnValidator = new IsbnValidator();
        boolean result = isbnValidator.ControlIsbn("2213725845");
        assertTrue(result);
    }

    @Test
    public void check_Invalid10Char_ISBNCode_IsValid(){
        IsbnValidator isbnValidator = new IsbnValidator();
        boolean result = isbnValidator.ControlIsbn("2213725846");
        assertFalse(result);
    }

    @Test
    public void check_Valid10Char_ISBNCodeEndWithX_IsValid(){
        IsbnValidator isbnValidator = new IsbnValidator();
        boolean result = isbnValidator.ControlIsbn("059045370X"/*"030640615X"*/);
        assertTrue(result);
    }

    @Test
    public void check_Invalid9Chars_ISBNCode_TrowError() {
        IsbnValidator isbnValidator = new IsbnValidator();
        assertThrows(InvalidIsbnLengthExeption.class, () -> isbnValidator.ControlIsbn("237126420"/*2*/));

    }

    @Test
    public void check_Invalid11Chars_ISBNCode_TrowError() {
        IsbnValidator isbnValidator = new IsbnValidator();
        assertThrows(InvalidIsbnLengthExeption.class, () -> isbnValidator.ControlIsbn("97823712640"/*45*/));

    }



    @Test
    public void check_Book_IsValid(){
        IsbnValidator isbnValidator = new IsbnValidator();
        boolean result = isbnValidator.ControlValidBook(new Book("2213725845"));

        assertTrue(result);


    }


    // to do
    /*
    Tester si j'ai un livre "valide*
        -> savoir si tout mes élément sont valide et remplis
        -si oui => passer le test
     */




}
