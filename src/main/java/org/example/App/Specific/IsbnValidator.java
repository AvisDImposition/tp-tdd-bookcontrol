package org.example.App.Specific;

import org.example.App.Eception.InvalidIsbnLengthExeption;
import org.example.App.Model.Book;


public class IsbnValidator
{
    public boolean ControlIsbn (String isbn)
    {
        int total = 0;

        if (isbn.length() != 10)
            throw new InvalidIsbnLengthExeption();

        for (int i = 0; i < isbn.length(); i++)
        {
            if (i ==9 && isbn.charAt(i)=='X')
            {
                total += 10;
                break;
            }
            total += Character.getNumericValue(isbn.charAt(i)) * (10-i);
        }
        if (total % 11 == 0)
            return true;

        return false;
    }


    public boolean ControlValidBook(Book book)
    {

        return ControlIsbn(book.getIsbn());
    }


}
